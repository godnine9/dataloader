﻿using System.IO;
using System.Data;
using ExcelDataReader;
using UnityEngine;
using System.Collections.Generic;
using System.Xml;
public static class DataManager
{
	public static Dictionary<string, Dictionary<string, string>> PlayerDictionary;
	public static Dictionary<string, Dictionary<string, string>> EnemyDictionary;

	//-----------------------------------------------------------------------------------------------------------
	//                                                    ExcelReadAndSet
	//-----------------------------------------------------------------------------------------------------------
	public static Dictionary<string, Dictionary<string, string>> ExcelReadAndSet(string iFilePaht)
	{
		Dictionary<string, Dictionary<string, string>> aResultDicitionnary = new Dictionary<string, Dictionary<string, string>>();

		FileStream stream = File.Open(Application.dataPath + iFilePaht, FileMode.Open, FileAccess.Read);
		IExcelDataReader excelDataReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

		DataSet result = excelDataReader.AsDataSet();
		int columns = result.Tables[0].Columns.Count;//列
		int rows = result.Tables[0].Rows.Count;//行

		for (int i = 0; i < rows; i++)
		{
			if (i > 0)
			{
				string aSearchID = string.Empty;
				Dictionary<string, string> aDictionary = new Dictionary<string, string>();
				for (int j = 0; j < columns; j++)
				{
					if (j == 0)
					{
						aSearchID = result.Tables[0].Rows[i][j].ToString();
					}
					else
					{
						aDictionary.Add(result.Tables[0].Rows[0][j].ToString(), result.Tables[0].Rows[i][j].ToString());
					}
				}
				aResultDicitionnary.Add(aSearchID, aDictionary);
			}
		}

		excelDataReader.Close();
		return aResultDicitionnary;
	}

	//-----------------------------------------------------------------------------------------------------------
	//                                                    JsonReadAndSet
	//-----------------------------------------------------------------------------------------------------------
	public static Dictionary<string, Dictionary<string, string>> JsonReadAndSet(string iFilePaht)
	{
		Dictionary<string, Dictionary<string, string>> aResultDicitionnary = new Dictionary<string, Dictionary<string, string>>();

		StreamReader aStreamReader = new StreamReader(Application.dataPath + iFilePaht);
		if (aStreamReader != null)
		{
			string aJson = aStreamReader.ReadToEnd();
			MyData loadedData = JsonUtility.FromJson<MyData>(aJson);
			Debug.Log(aJson);
			if (loadedData != null)
			{
				for (int aIndex = 0; aIndex < loadedData.Enemise.Length; aIndex++)
				{
					string aSearchID = loadedData.Enemise[aIndex].ID;
					Dictionary<string, string> aDictionary = new Dictionary<string, string>();
					Debug.Log(loadedData.Enemise[aIndex].HP);
					aDictionary.Add(DataID.HP_NODE, loadedData.Enemise[aIndex].HP);
					aDictionary.Add(DataID.ATK_NODE, loadedData.Enemise[aIndex].ATK);
					aResultDicitionnary.Add(aSearchID, aDictionary);
				}
			}
		}

		return aResultDicitionnary;
	}

	//-----------------------------------------------------------------------------------------------------------
	//                                                    XMLReadAndSet
	//-----------------------------------------------------------------------------------------------------------
	public static Dictionary<string, Dictionary<string, string>> XMLReadAndSet(string iFilePaht)
	{
		Dictionary<string, Dictionary<string, string>> aResultDicitionnary = new Dictionary<string, Dictionary<string, string>>();

		XmlDocument aXml = new XmlDocument();
		aXml.Load(Application.dataPath + iFilePaht);
		XmlNodeList aXMLNodeList = aXml.SelectSingleNode(DataID.ROOT_NODE).ChildNodes;
		

		for(int aXNLElements = 0; aXNLElements < aXMLNodeList.Count;aXNLElements++)
		{
			string aSearchID = aXMLNodeList[aXNLElements].SelectSingleNode(DataID.ID_NODE).InnerText;
			Dictionary<string, string> aDictionary = new Dictionary<string, string>();

			aDictionary.Add(DataID.HP_NODE, aXMLNodeList[aXNLElements].SelectSingleNode(DataID.HP_NODE).InnerText);
			aDictionary.Add(DataID.ATK_NODE, aXMLNodeList[aXNLElements].SelectSingleNode(DataID.ATK_NODE).InnerText);
			aResultDicitionnary.Add(aSearchID, aDictionary);
		}

		return aResultDicitionnary;
	}
}

public static class DataID
{
	//Excel
	public const string EXCEL_PLAYER_DATA_PATH = "/Resources/PlayerMasterData.xlsx";
	public const string EXCEL_ENEMY_DATA_PATH = "/Resources/EnemyMasterData.xlsx";

	//Json
	public const string JSON_PLAYER_DATA_PATH = "/Resources/EnemyMasterData.json";
	public const string JSON_ENEMY_DATA_PATH = "/Resources/EnemyMasterData.json";

	//XML 
	public const string XML_PLAYER_DATA_PATH = "/Resources/EnemyMasterData.xml";
	public const string XML_ENEMY_DATA_PATH = "/Resources/EnemyMasterData.xml";

	public const string ROOT_NODE = "Root";
	public const string ID_NODE = "ID";
	public const string HP_NODE = "HP";
	public const string ATK_NODE = "ATK";
}


[System.Serializable]
public class MyData
{
	public Refencenes[] Enemise;
}

[System.Serializable]
public class Refencenes
{
	public string ID;
	public string HP;
	public string ATK;
}