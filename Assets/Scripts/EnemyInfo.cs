﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInfo : MonoBehaviour {

	public string ID;
	public int HP;
	public int ATK;

	// Use this for initialization
	void Start()
	{

		/*
		DataManager.EnemyDictionary = DataManager.ExcelReadAndSet(DataID.EXCEL_ENEMY_DATA_PATH);
		if (DataManager.EnemyDictionary.ContainsKey(ID))
		{
			Dictionary<string, string> aDictionary = DataManager.EnemyDictionary[ID];
			HP = int.Parse(aDictionary[DataID.ID_NODE]);
			ATK = int.Parse(aDictionary[DataID.ATK_NODE]);
		}
		*/
		/*
		DataManager.EnemyDictionary = DataManager.JsonReadAndSet(DataID.JSON_ENEMY_DATA_PATH);
		if (DataManager.EnemyDictionary.ContainsKey(ID))
		{
			Dictionary<string, string> aDictionary = DataManager.EnemyDictionary[ID];
			HP = int.Parse(aDictionary[DataID.ID_NODE]);
			ATK = int.Parse(aDictionary[DataID.ATK_NODE]);
		}
		*/
		DataManager.EnemyDictionary = DataManager.XMLReadAndSet(DataID.XML_ENEMY_DATA_PATH);
		if (DataManager.EnemyDictionary.ContainsKey(ID))
		{
			Dictionary<string, string> aDictionary = DataManager.EnemyDictionary[ID];
			HP = int.Parse(aDictionary[DataID.HP_NODE]);
			ATK = int.Parse(aDictionary[DataID.ATK_NODE]);
		}
	}
	}